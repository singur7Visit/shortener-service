package com.infobip.shortener.constants;

public class JdbcConstants {
    public static final String SELECT_URL_QUERY = "SELECT id, long_url, redirect_code FROM links WHERE id=:id";
    public static final String CREATE_SHORT_LINK_QUERY = "INSERT INTO links (long_url, redirect_code, account_id) VALUES (:long_url,:redirect_code,:account_id)";

    public static final String SELECT_ACCOUNT_BY_ACCOUNT_ID = "SELECT id, username, password FROM accounts WHERE username=:username";
    public static final String CREATE_ACCOUNT_QUERY= "INSERT INTO accounts (username, password) VALUES (:username,:password)";
    public static final String INCREMENT_UPDATE_STATISTIC = "UPDATE statistics SET redirects=:redirects WHERE redirects=(:redirects-1) and link=:link";
    public static final String CREATE_STATISTIC = "INSERT INTO statistics (link,redirects) VALUES (:link,:redirects)";
    public static final String SELECT_STATISTIC = "SELECT link,redirects FROM statistics WHERE link=:link";
    public static final String SELECT_STATISTIC_FOR_ACCOUNT = "SELECT link,long_url,redirects FROM statistics JOIN links ON statistics.link = links.id JOIN accounts ON accounts.id=links.account_id WHERE accounts.username=:username";


    public static final String LONG_URL_PARAM = "long_url";
    public static final String REDIRECT_CODE_PARAM = "redirect_code";
    public static final String ID_PARAM = "id";
    public static final String LINK_PARAM = "link";
    public static final String REDIRECTS_PARAM = "redirects";
    public static final String ACCOUNT_ID_PARAM = "account_id";
    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "password";
}

package com.infobip.shortener.service.impl;

import com.infobip.shortener.dao.AccountDao;
import com.infobip.shortener.entity.AccountEntity;
import com.infobip.shortener.security.AccountDetails;
import com.infobip.shortener.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service("authService")
public class AuthenticationServiceImpl implements AuthenticationService,UserDetailsService {

    @Autowired
    private AccountDao accountDao;

    @Override
    public UserDetails loadUserByUsername(String accountId) throws UsernameNotFoundException {
        AccountEntity accountEntity = accountDao.getAccount(accountId);
        if(accountEntity == null) {
            throw new UsernameNotFoundException("User doesn't exist");
        }
        return createUserDetails(accountEntity);
    }

    private UserDetails createUserDetails(AccountEntity accountEntity) {
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
        return new AccountDetails(accountEntity, Collections.singleton(authority));
    }

    @Override
    public AccountDetails getCurrentAccountDetails() {
        Authentication authentication =  SecurityContextHolder.getContext().getAuthentication();
        return (AccountDetails) authentication.getPrincipal();
    }
}

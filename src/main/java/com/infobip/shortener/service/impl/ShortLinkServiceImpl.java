package com.infobip.shortener.service.impl;

import com.infobip.shortener.dao.ShortLinkDao;
import com.infobip.shortener.entity.UrlEntity;
import com.infobip.shortener.security.AccountDetails;
import com.infobip.shortener.service.AuthenticationService;
import com.infobip.shortener.service.ShortLinkService;
import com.infobip.shortener.util.Preconditions;
import com.infobip.shortener.util.LinkUtil;
import org.infinispan.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShortLinkServiceImpl implements ShortLinkService {

    @Autowired
    private Cache<String, UrlEntity> urlCache;
    @Autowired
    private ShortLinkDao shortLinkDao;
    @Autowired
    private AuthenticationService authService;

    @Override
    public String createShortLink(String longUrl, Integer redirectCode) {
        Preconditions.requireMandatoryAttribute("url",longUrl);
        AccountDetails accountDetails = authService.getCurrentAccountDetails();
        UrlEntity urlEntity = toEntity(longUrl,redirectCode, accountDetails.getAccountEntity().getId());
        urlEntity = shortLinkDao.createUrlEntity(urlEntity);
        return cacheEntity(urlEntity);
    }

    @Override
    public UrlEntity getUrl(String shortUrl) {
        Preconditions.requireMandatoryAttribute("url",shortUrl);
        Optional<UrlEntity> cacheUrlEntity = Optional.ofNullable(urlCache.get(shortUrl));
        return cacheUrlEntity.map(this::copyEntity).orElseGet(() -> getInternalUrlEntity(shortUrl));

    }

    private UrlEntity getInternalUrlEntity(String shortUrl) {
        Long id = LinkUtil.getIdByShortLink(shortUrl);
        UrlEntity urlEntity = (id == null ? null : shortLinkDao.getLongUrl(id));
        if(urlEntity != null) {
            urlCache.putIfAbsent(shortUrl, urlEntity);
        }
        return copyEntity(urlEntity);
    }

    private UrlEntity copyEntity(UrlEntity urlEntity) {
        return urlEntity == null ? null : toEntity(urlEntity.getUrl(), urlEntity.getStatus(), urlEntity.getAccountId());
    }

    private UrlEntity toEntity(String url, int redirectCode, Long accountId) {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setUrl(url);
        urlEntity.setStatus(redirectCode);
        urlEntity.setAccountId(accountId);
        return urlEntity;
    }

    private String cacheEntity(UrlEntity urlEntity) {
        String shortLink = LinkUtil.getShortLinkById(urlEntity.getId());
        urlCache.putIfAbsent(shortLink, urlEntity);
        return shortLink;
    }
}

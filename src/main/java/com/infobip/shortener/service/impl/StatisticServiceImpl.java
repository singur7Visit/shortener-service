package com.infobip.shortener.service.impl;

import com.infobip.shortener.dao.StatisticDao;
import com.infobip.shortener.entity.StatisticEntity;
import com.infobip.shortener.entity.StatisticForAccountEntity;
import com.infobip.shortener.service.StatisticService;
import com.infobip.shortener.util.Preconditions;
import com.infobip.shortener.util.LinkUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    private StatisticDao statisticDao;

    @Override
    public void incrementStatistic(String url) {
        Preconditions.requireMandatoryAttribute("url",url);
        Long link = LinkUtil.getIdByShortLink(url);
        StatisticEntity statisticEntity = statisticDao.getStatisticEntityByLink(link);
        if(statisticEntity == null) {
            statisticEntity = new StatisticEntity();
            statisticEntity.setLink(link);
            statisticEntity.setRedirects(1L);
            statisticDao.createStatisticEntity(statisticEntity);
        }
        else {
            statisticEntity.setRedirects(statisticEntity.getRedirects() + 1);
            while (statisticDao.update(statisticEntity) == 0) {
                statisticEntity = statisticDao.getStatisticEntityByLink(link);
                statisticEntity.setRedirects(statisticEntity.getRedirects() + 1);
            }
        }

    }

    @Override
    public Collection<StatisticForAccountEntity> getStatisticForAccount(String accountId) {
        Preconditions.requireMandatoryAttribute("accountId",accountId);
        return statisticDao.getStatisticForAccount(accountId);
    }



}

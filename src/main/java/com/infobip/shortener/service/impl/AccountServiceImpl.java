package com.infobip.shortener.service.impl;

import com.infobip.shortener.dao.AccountDao;
import com.infobip.shortener.entity.AccountEntity;
import com.infobip.shortener.service.AccountService;
import com.infobip.shortener.util.Preconditions;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Override
    public AccountEntity createAccount(String accountId) {
        Preconditions.requireMandatoryAttribute("accountId",accountId);
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountId(accountId);
        accountEntity.setPassword(generatePassword());
        return accountDao.createAccount(accountEntity);
    }

    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(8);
    }


}

package com.infobip.shortener.service;

import com.infobip.shortener.entity.UrlEntity;


/**
 *  Service to manage short links
 */
public interface ShortLinkService {

    /**
     * Create short link
     * @param longUrl long url
     * @param redirectCode redirect code
     * @return generated short link
     */
    String createShortLink(String longUrl, Integer redirectCode);

    /**
     * Get long url and redirect code by short link
     * @param shortUrl - short link
     * @return url entity with long url and redirect code
     */
    UrlEntity getUrl(String shortUrl);
}

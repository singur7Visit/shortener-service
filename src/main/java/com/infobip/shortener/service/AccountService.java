package com.infobip.shortener.service;

import com.infobip.shortener.entity.AccountEntity;

/**
 * Service to manage accounts
 */
public interface AccountService {
    /**
     * Create user account
     * @param accountId - account id
     * @return created account entity
     */
    AccountEntity createAccount(String accountId);
}

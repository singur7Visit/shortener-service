package com.infobip.shortener.service;

import com.infobip.shortener.entity.StatisticForAccountEntity;

import java.util.Collection;

/**
 * Service to collect statistic about links
 */
public interface StatisticService {

    /**
     * Increase count redirects for short url
     * @param url - short url
     */
    void incrementStatistic(String url);

    /**
     * Get statistic for account
     * @param accountId - account id
     * @return current statistic by account id
     */
    Collection<StatisticForAccountEntity> getStatisticForAccount(String accountId);
}

package com.infobip.shortener.service;

import com.infobip.shortener.security.AccountDetails;

/**
 * Service to authenticate and get information about principal
 */

public interface AuthenticationService {
    /**
     * Get current principal
     * @return account details with roles
     */
    AccountDetails getCurrentAccountDetails();
}

package com.infobip.shortener.configuration.cache;

import com.infobip.shortener.entity.UrlEntity;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig {

    @Bean
    public Cache<String, UrlEntity> urlCache() {
        return getCache("urlCache");
    }

    private <K,V> Cache<K,V> getCache(String name) {
        org.infinispan.configuration.cache.Configuration configuration = createCacheConfig();
        EmbeddedCacheManager manager = new DefaultCacheManager(configuration);
        return manager.getCache(name);
    }

    private org.infinispan.configuration.cache.Configuration createCacheConfig() {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        return builder.memory()
                .eviction()
                .strategy(EvictionStrategy.LIRS)
                .size(1000)
                .storeAsBinary()
                    .enable()
                .build();
    }

}

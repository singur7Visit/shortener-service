package com.infobip.shortener.api;

import com.infobip.shortener.entity.UrlEntity;
import com.infobip.shortener.event.StatisticEvent;
import com.infobip.shortener.service.ShortLinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to get long url by short url
 */

@RestController
public class ShortUrlController {

    @Autowired
    private ShortLinkService shortLinkService;
    @Autowired
    private ApplicationEventPublisher publisher;

    @RequestMapping("/{url}")
    public ResponseEntity<String> handleShortLink(@PathVariable("url") String shortUrl) {
        UrlEntity urlEntity = shortLinkService.getUrl(shortUrl);
        if(urlEntity != null) {
            updateStatistic(shortUrl);
        }
        return createResponse(urlEntity);
    }

    private ResponseEntity<String> createResponse(UrlEntity urlEntity) {
        if(urlEntity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, urlEntity.getUrl());
        return new ResponseEntity<>(headers, HttpStatus.valueOf(urlEntity.getStatus()));
    }

    private void updateStatistic(String shortUrl) {
        publisher.publishEvent(new StatisticEvent(shortUrl));
    }
}

package com.infobip.shortener.api;

import com.infobip.shortener.dto.request.RegisterRequestDto;
import com.infobip.shortener.dto.response.RegisterResponseDto;
import com.infobip.shortener.exception.SameContextException;
import com.infobip.shortener.service.ShortLinkService;
import com.infobip.shortener.util.LinkUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller to register new short link
 */

@RestController
@RequestMapping("/register")
@Secured("ROLE_USER")
public class RegisterUrlController {

    @Autowired
    private ShortLinkService shortLinkService;

    @RequestMapping(method = RequestMethod.POST)
    public RegisterResponseDto register(@RequestBody RegisterRequestDto registerRequest, HttpServletRequest request) {
        if(LinkUtil.inTheSameContext(registerRequest.getUrl(), request)) {
            throw new SameContextException();
        }
        String shortLink = shortLinkService.createShortLink(registerRequest.getUrl(),registerRequest.getRedirectType());
        RegisterResponseDto registerResponse = new RegisterResponseDto();
        registerResponse.setShortUrl(LinkUtil.toFullLink(shortLink,request));
        return registerResponse;
    }
}

package com.infobip.shortener.api;

import com.infobip.shortener.dto.request.AccountRequestDto;
import com.infobip.shortener.dto.response.AccountResponseDto;
import com.infobip.shortener.entity.AccountEntity;
import com.infobip.shortener.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to register new user
 */

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.POST)
    public AccountResponseDto createAccount(@RequestBody AccountRequestDto accountRequest) {
        AccountResponseDto response = new AccountResponseDto();
        AccountEntity accountEntity = accountService.createAccount(accountRequest.getAccountId());
        response.setSuccess(true);
        response.setPassword(accountEntity.getPassword());
        response.setDescription("Your account is opened");
        return response;
    }
}

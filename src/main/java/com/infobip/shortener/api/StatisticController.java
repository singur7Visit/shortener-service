package com.infobip.shortener.api;


import com.infobip.shortener.entity.StatisticForAccountEntity;
import com.infobip.shortener.service.StatisticService;
import com.infobip.shortener.util.LinkUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to get statistic by account
 */

@RestController
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET)
    @PreAuthorize("#accountId == authentication.name")
    public List<String> getStatistic(@PathVariable("accountId") String accountId, Authentication authentication,
                                     HttpServletRequest request) {
        return statisticService.getStatisticForAccount(accountId).stream().map(entity -> toString(entity, request))
                .collect(Collectors.toList());
    }

    private String toString(StatisticForAccountEntity entity, HttpServletRequest request) {
        return String.join(":", LinkUtil.toFullLink(entity.getShortUrl(), request) ,
                entity.getLongUrl(),String.valueOf(entity.getRedirects()));
    }
}

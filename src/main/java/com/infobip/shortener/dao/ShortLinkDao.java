package com.infobip.shortener.dao;

import com.infobip.shortener.entity.UrlEntity;

/**
 * Dao to get and register new short link
 */

public interface ShortLinkDao {

    /**
     * Get url entity by url id
     * @param id - url id
     * @return url entity
     */
    UrlEntity getLongUrl(Long id);

    /**
     * Create url entity in persistence storage
     * @param urlEntity - url entity which will be created
     * @return url entity with generated id
     */
    UrlEntity createUrlEntity(UrlEntity urlEntity);

}

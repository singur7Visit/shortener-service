package com.infobip.shortener.dao.impl;

import com.infobip.shortener.dao.AccountDao;
import com.infobip.shortener.entity.AccountEntity;
import com.infobip.shortener.exception.UserExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.infobip.shortener.constants.JdbcConstants.*;

@Repository
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public AccountEntity createAccount(AccountEntity accountEntity)
    {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(CREATE_ACCOUNT_QUERY, extractToSqlParameterSource(accountEntity), keyHolder);
            accountEntity.setId(keyHolder.getKey().longValue());
            return accountEntity;
        }
        catch (DuplicateKeyException e) {
            throw new UserExistsException(accountEntity.getAccountId(), e);
        }
    }

    private SqlParameterSource extractToSqlParameterSource(AccountEntity accountEntity) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(USERNAME_PARAM, accountEntity.getAccountId());
        sqlParameterSource.addValue(PASSWORD_PARAM, accountEntity.getPassword());
        return sqlParameterSource;
    }

    @Override
    public AccountEntity getAccount(String accountId) {
        return jdbcTemplate.query(SELECT_ACCOUNT_BY_ACCOUNT_ID,
                new MapSqlParameterSource(USERNAME_PARAM, accountId), this::extractAccountEntity);
    }

    private AccountEntity extractAccountEntity(ResultSet resultSet) throws SQLException {
        AccountEntity accountEntity = null;
        if(resultSet.next()) {
            accountEntity = new AccountEntity();
            accountEntity.setId(resultSet.getLong(ID_PARAM));
            accountEntity.setAccountId(resultSet.getString(USERNAME_PARAM));
            accountEntity.setPassword(resultSet.getString(PASSWORD_PARAM));
        }
        return accountEntity;
    }

}

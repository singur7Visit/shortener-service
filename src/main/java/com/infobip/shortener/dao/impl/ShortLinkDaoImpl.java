package com.infobip.shortener.dao.impl;

import com.infobip.shortener.dao.ShortLinkDao;
import com.infobip.shortener.entity.UrlEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.infobip.shortener.constants.JdbcConstants.*;

@Repository
public class ShortLinkDaoImpl implements ShortLinkDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public UrlEntity getLongUrl(Long id) {
        return jdbcTemplate.query(SELECT_URL_QUERY, new MapSqlParameterSource(ID_PARAM, id), this::extractUrlEntity);
    }

    private UrlEntity extractUrlEntity(ResultSet resultSet) throws SQLException, DataAccessException {
        UrlEntity urlEntity = null;
        if (resultSet.next()) {
            String longUrl = resultSet.getString(LONG_URL_PARAM);
            int redirectCode = resultSet.getInt(REDIRECT_CODE_PARAM);
            urlEntity = new UrlEntity();
            urlEntity.setUrl(longUrl);
            urlEntity.setStatus(redirectCode);
        }
        return urlEntity;
    }

    @Override
    public UrlEntity createUrlEntity(UrlEntity urlEntity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(CREATE_SHORT_LINK_QUERY, extractToSqlParameterSource(urlEntity), keyHolder);
        urlEntity.setId(keyHolder.getKey().longValue());
        return urlEntity;
    }

    private SqlParameterSource extractToSqlParameterSource(UrlEntity urlEntity) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(LONG_URL_PARAM, urlEntity.getUrl());
        sqlParameterSource.addValue(REDIRECT_CODE_PARAM, urlEntity.getStatus());
        sqlParameterSource.addValue(ACCOUNT_ID_PARAM, urlEntity.getAccountId());
        return sqlParameterSource;
    }
}


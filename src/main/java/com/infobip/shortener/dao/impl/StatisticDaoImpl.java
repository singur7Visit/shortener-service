package com.infobip.shortener.dao.impl;

import com.infobip.shortener.dao.StatisticDao;
import com.infobip.shortener.entity.StatisticEntity;
import com.infobip.shortener.entity.StatisticForAccountEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import static com.infobip.shortener.constants.JdbcConstants.*;

@Repository
public class StatisticDaoImpl implements StatisticDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public int update(StatisticEntity statisticEntity) {
        return jdbcTemplate.update(INCREMENT_UPDATE_STATISTIC, statisticSqlParameterSource(statisticEntity));
    }

    private SqlParameterSource statisticSqlParameterSource(StatisticEntity statisticEntity) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(LINK_PARAM, statisticEntity.getLink());
        sqlParameterSource.addValue(REDIRECTS_PARAM, statisticEntity.getRedirects());
        return sqlParameterSource;
    }

    @Override
    public StatisticEntity createStatisticEntity(StatisticEntity statisticEntity) {
        jdbcTemplate.update(CREATE_STATISTIC, new BeanPropertySqlParameterSource(statisticEntity));
        return statisticEntity;
    }

    @Override
    public StatisticEntity getStatisticEntityByLink(Long link) {
        return jdbcTemplate.query(SELECT_STATISTIC, new MapSqlParameterSource(LINK_PARAM, link), this::extractData);
    }

    @Override
    public Collection<StatisticForAccountEntity> getStatisticForAccount(String username) {
        return jdbcTemplate.query(SELECT_STATISTIC_FOR_ACCOUNT, new MapSqlParameterSource(USERNAME_PARAM, username),
                this::mapRow);
    }

    private StatisticEntity extractData(ResultSet resultSet) throws SQLException {
        StatisticEntity statisticEntity = null;
        if(resultSet.next()) {
            statisticEntity = new StatisticEntity();
            statisticEntity.setLink(resultSet.getLong(LINK_PARAM));
            statisticEntity.setRedirects(resultSet.getLong(REDIRECTS_PARAM));
        }
        return statisticEntity;
    }

    private StatisticForAccountEntity mapRow(ResultSet resultSet, int row) throws SQLException {
        StatisticForAccountEntity statisticForAccountEntity = new StatisticForAccountEntity();
        statisticForAccountEntity.setShortUrl(resultSet.getLong(LINK_PARAM));
        statisticForAccountEntity.setRedirects(resultSet.getLong(REDIRECTS_PARAM));
        statisticForAccountEntity.setLongUrl(resultSet.getString(LONG_URL_PARAM));
        return statisticForAccountEntity;
    }

}

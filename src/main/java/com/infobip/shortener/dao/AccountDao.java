package com.infobip.shortener.dao;

import com.infobip.shortener.entity.AccountEntity;

/**
 * Dao to get and create info about new user
 */
public interface AccountDao {

    /**
     * Create new account in persistence storage
     * @param accountEntity - entity which will be created
     * @return created account entity with generated id
     */
    AccountEntity createAccount(AccountEntity accountEntity);

    /**
     * Get account entity by account id
     * @param accountId - account id
     * @return account entity for account id
     */
    AccountEntity getAccount(String accountId);
}

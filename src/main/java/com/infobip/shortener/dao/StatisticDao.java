package com.infobip.shortener.dao;

import com.infobip.shortener.entity.StatisticEntity;
import com.infobip.shortener.entity.StatisticForAccountEntity;

import java.util.Collection;

/**
 * Dao to get and update statistic for account
 */
public interface StatisticDao {

    /**
     * Update statistic entity
     * @param statisticEntity - new statistic entity
     * @return 1 if update was successfully otherwise - 0
     */
    int update(StatisticEntity statisticEntity);

    /**
     * Create statistic entity in persistence storage
     * @param statisticEntity - new statistic entity
     * @return saved statistic entity
     */
    StatisticEntity createStatisticEntity(StatisticEntity statisticEntity);

    /**
     * Get statistic entity by link id
     * @param link - link id
     * @return - found statistic entity
     */
    StatisticEntity getStatisticEntityByLink(Long link);

    /**
     * Get statistic for user
     * @param username - account id
     * @return statistic entities for account
     */
    Collection<StatisticForAccountEntity> getStatisticForAccount(String username);
}

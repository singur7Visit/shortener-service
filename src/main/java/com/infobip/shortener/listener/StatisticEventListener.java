package com.infobip.shortener.listener;

import com.infobip.shortener.event.StatisticEvent;
import com.infobip.shortener.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Asynchronous listener to update statistic
 */

@Component
public class StatisticEventListener {

    @Autowired
    private StatisticService statisticService;

    @EventListener
    @Async
    public void handleEvent(StatisticEvent statisticEvent) {
        statisticService.incrementStatistic(statisticEvent.getShortUrl());
    }
}

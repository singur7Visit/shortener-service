package com.infobip.shortener.handler;

import com.infobip.shortener.dto.response.AccountResponseDto;
import com.infobip.shortener.dto.response.InfoResponseDto;
import com.infobip.shortener.exception.MandatoryAttributeException;
import com.infobip.shortener.exception.SameContextException;
import com.infobip.shortener.exception.UserExistsException;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

/**
 * Controller to handle application exceptions
 */

@ControllerAdvice
public class AppExceptionHandler {

    private static final Logger LOG = Logger.getLogger(AppExceptionHandler.class);

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(UserExistsException.class)
    public @ResponseBody AccountResponseDto userAlreadyExist(UserExistsException e) {
        LOG.error(e, e);
        AccountResponseDto accountResponseDto = new AccountResponseDto();
        accountResponseDto.setDescription(MessageFormat.format("Account with ID : {0} already exists", e.getUserId()));
        return accountResponseDto;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(MandatoryAttributeException.class)
    public @ResponseBody InfoResponseDto mandatoryAttributeException(MandatoryAttributeException e) {
        LOG.error(e, e);
        InfoResponseDto infoResponseDto = new InfoResponseDto();
        infoResponseDto.setDescription(MessageFormat.format("Mandatory attribute {0} is missed",
                e.getMandatoryAttributeName()));
        return infoResponseDto;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(SameContextException.class)
    public @ResponseBody InfoResponseDto sameContextException(SameContextException e) {
        LOG.error(e, e);
        InfoResponseDto infoResponseDto = new InfoResponseDto();
        infoResponseDto.setDescription("Url already in the short link server");
        return infoResponseDto;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataAccessException.class)
    public @ResponseBody String dataAccessFail(DataAccessException e) {
        LOG.error(e, e);
        return "Problem with access to DB";
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public @ResponseBody String otherException(Exception e) {
        LOG.error(e, e);
        return "Unexpected exception";
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AuthenticationException.class)
    public @ResponseBody InfoResponseDto authenticationError(AuthenticationException e) {
        LOG.error(e, e);
        InfoResponseDto infoResponseDto = new InfoResponseDto();
        infoResponseDto.setDescription("Authentication error");
        return infoResponseDto;
    }


}

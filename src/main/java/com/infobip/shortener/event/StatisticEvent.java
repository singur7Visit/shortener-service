package com.infobip.shortener.event;

public class StatisticEvent {

    private final String shortUrl;

    public StatisticEvent(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }
}

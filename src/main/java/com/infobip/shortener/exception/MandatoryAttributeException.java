package com.infobip.shortener.exception;

public class MandatoryAttributeException extends RuntimeException {

    private final String mandatoryAttributeName;

    public MandatoryAttributeException(String mandatoryAttributeName) {
        this.mandatoryAttributeName = mandatoryAttributeName;
    }

    public String getMandatoryAttributeName() {
        return mandatoryAttributeName;
    }
}

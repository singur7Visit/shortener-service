package com.infobip.shortener.exception;

public class UserExistsException extends RuntimeException {

    private final String userId;

    public UserExistsException(String userId) {
        this.userId = userId;
    }

    public UserExistsException(String userId, Throwable e) {
        super(e);
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}

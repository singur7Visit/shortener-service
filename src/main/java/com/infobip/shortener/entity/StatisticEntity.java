package com.infobip.shortener.entity;

public class StatisticEntity {

    private Long link;
    private Long redirects;

    public Long getLink() {
        return link;
    }

    public void setLink(Long link) {
        this.link = link;
    }

    public Long getRedirects() {
        return redirects;
    }

    public void setRedirects(Long redirects) {
        this.redirects = redirects;
    }
}

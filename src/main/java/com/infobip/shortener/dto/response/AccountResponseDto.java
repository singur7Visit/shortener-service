package com.infobip.shortener.dto.response;

public class AccountResponseDto extends InfoResponseDto {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

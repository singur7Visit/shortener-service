package com.infobip.shortener.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountRequestDto {

    @JsonProperty(value = "AccountId")
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}

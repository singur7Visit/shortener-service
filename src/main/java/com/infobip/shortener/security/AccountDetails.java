package com.infobip.shortener.security;

import com.infobip.shortener.entity.AccountEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;


public class AccountDetails extends User {

    private final AccountEntity accountEntity;

    public AccountDetails(AccountEntity accountEntity, Collection<? extends GrantedAuthority> authorities) {
        super(accountEntity.getAccountId(), accountEntity.getPassword(), authorities);
        this.accountEntity = accountEntity;
    }

    public AccountEntity getAccountEntity() {
        return accountEntity;
    }
}

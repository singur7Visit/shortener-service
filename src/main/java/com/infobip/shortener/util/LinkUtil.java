package com.infobip.shortener.util;

import org.apache.commons.lang3.StringUtils;
import org.hashids.Hashids;

import javax.servlet.http.HttpServletRequest;

public class LinkUtil {

    private static final Hashids HASHIDS = new Hashids("",6);

    public static String getShortLinkById(Long id) {
        return id == null ? null : HASHIDS.encode(id);
    }

    public static Long getIdByShortLink(String shortLink) {
        try {
            long[] result = HASHIDS.decode(shortLink);
            return result.length == 0 ? null : result[0];
        }
        catch (Exception e) {
            // ignore because shortLink has not valid hash
            return null;
        }
    }

    public static String toFullLink(Long shortUrl, HttpServletRequest request) {
        return toFullLink(getShortLinkById(shortUrl),request);
    }

    public static String toFullLink(String shortUrl, HttpServletRequest request) {
        return request.getScheme() + "://" +
                request.getServerName() +
                ":" + request.getServerPort() + "/"
                + (StringUtils.isNoneEmpty(request.getContextPath()) ? request.getContextPath()  + "/" : "") +
                shortUrl;
    }

    public static boolean inTheSameContext(String url, HttpServletRequest request) {
        return StringUtils.contains(url,request.getScheme() + "://" +
                request.getServerName() +
                ":" + request.getServerPort());
    }

}

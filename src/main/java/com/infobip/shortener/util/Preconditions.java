package com.infobip.shortener.util;

import com.infobip.shortener.exception.MandatoryAttributeException;

public class Preconditions {

    public static void requireMandatoryAttribute(String name, Object value) {
        if(value == null)
            throw new MandatoryAttributeException(name);
    }
}

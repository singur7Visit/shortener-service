package com.infobip.shortener.view;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelpViewController {

    @RequestMapping(value = "/help", method = RequestMethod.GET)
    public String help() {
        return "index";
    }
}

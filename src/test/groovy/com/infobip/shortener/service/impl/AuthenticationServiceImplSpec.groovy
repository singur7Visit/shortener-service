package com.infobip.shortener.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.infobip.shortener.dao.AccountDao
import com.infobip.shortener.entity.AccountEntity
import org.springframework.security.core.userdetails.UsernameNotFoundException
import spock.lang.Specification

class AuthenticationServiceImplSpec extends Specification {

    @Collaborator
    AccountDao accountDao = Mock(AccountDao)

    @Subject
    AuthenticationServiceImpl authService = new AuthenticationServiceImpl()

    String testAccount = "testAccount";
    String password = "testPassword"

    void "when account is loaded - userDetails is not null"() {
        setup:

            def accountEntity = new AccountEntity(accountId: testAccount, password: password)
            accountDao.getAccount(_ as String) >> accountEntity
        when:
            def userDetails = authService.loadUserByUsername(testAccount)
        then:
            testAccount == userDetails.username
    }

    void "when account is loaded and account doesn't exist - then throw UsernameNotFoundException"() {
        when:
            authService.loadUserByUsername(testAccount)
        then:
            thrown UsernameNotFoundException
    }

}

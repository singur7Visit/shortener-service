package com.infobip.shortener.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.infobip.shortener.dao.ShortLinkDao
import com.infobip.shortener.entity.AccountEntity
import com.infobip.shortener.entity.UrlEntity
import com.infobip.shortener.security.AccountDetails
import com.infobip.shortener.service.AuthenticationService
import com.infobip.shortener.util.LinkUtil
import org.infinispan.Cache
import spock.lang.Specification

class ShortLinkServiceImplSpec extends Specification {

    @Collaborator
    Cache<String, UrlEntity> urlCache = Mock(Cache)
    Map<String, UrlEntity> mockUrlCache = new HashMap<>()

    AccountEntity accountEntity = new AccountEntity(accountId: "testAccount", password: "testPassword")
    AccountDetails accountDetails = new AccountDetails(accountEntity, Collections.emptyList())
    UrlEntity urlEntity = new UrlEntity(id: 1, url: "long_url", status: 301)

    @Collaborator
    AuthenticationService authService = Mock(AuthenticationService)

    @Collaborator
    ShortLinkDao shortLinkDao = Mock(ShortLinkDao)

    @Subject
    ShortLinkServiceImpl shortLinkService = new ShortLinkServiceImpl()

    def setup() {
        urlCache.putIfAbsent(_ as String, _ as UrlEntity) >>
        { String key, UrlEntity value ->
            mockUrlCache.put(key, value)
        }

        urlCache.get(_ as String) >>
                { String key ->
                    return mockUrlCache.get(key)
                }
        authService.getCurrentAccountDetails() >> accountDetails
    }

    void "when short link is created - value is add to cache"() {
        setup:
            shortLinkDao.createUrlEntity(_ as UrlEntity) >> urlEntity
        when:
            String shortUrl = shortLinkService.createShortLink("long_url",301)
        then:
            mockUrlCache.get(shortUrl) != null
    }

    void "when get long url - dao return long url and put cache"() {
        setup:
            shortLinkDao.getLongUrl(1) >> urlEntity
        when:
            String shortUrl = LinkUtil.getShortLinkById(1)
            UrlEntity resultUrlEntity = shortLinkService.getUrl(shortUrl)
        then:
            resultUrlEntity.url == urlEntity.url
    }

    void "when get long url ant cache contains long url - dao doesn't called"() {
        setup:
            String shortUrl = LinkUtil.getShortLinkById(1)
            mockUrlCache.put(shortUrl, urlEntity)
        when:
            UrlEntity resultUrlEntity = shortLinkService.getUrl(shortUrl)
        then:
            resultUrlEntity.url == urlEntity.url
            0 * shortLinkDao.getLongUrl(_ as Long)
    }
}

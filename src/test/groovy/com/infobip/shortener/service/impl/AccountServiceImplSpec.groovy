package com.infobip.shortener.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.infobip.shortener.dao.AccountDao
import com.infobip.shortener.entity.AccountEntity
import com.infobip.shortener.exception.MandatoryAttributeException
import spock.lang.Specification

class AccountServiceImplSpec extends Specification {

    @Collaborator
    AccountDao accountDao = Mock(AccountDao)

    @Subject
    AccountServiceImpl accountService = new AccountServiceImpl()


    void "when create account - account dao is called"() {
        when:
            accountService.createAccount("testAccount")
        then:
            1 * accountDao.createAccount(_ as AccountEntity)
    }

    void "when create account id is null - throw exception"() {
        when:
            accountService.createAccount(null)
        then:
            thrown MandatoryAttributeException
    }


}

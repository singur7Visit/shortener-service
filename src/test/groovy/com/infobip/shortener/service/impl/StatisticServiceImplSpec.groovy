package com.infobip.shortener.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.infobip.shortener.dao.StatisticDao
import com.infobip.shortener.entity.StatisticEntity
import com.infobip.shortener.util.LinkUtil
import spock.lang.Specification

class StatisticServiceImplSpec extends Specification {

    @Collaborator
    StatisticDao statisticDao = Mock(StatisticDao)

    @Subject
    StatisticServiceImpl statisticService = new StatisticServiceImpl()

    StatisticEntity statisticEntity = new StatisticEntity(redirects: 1)

    def setup() {
        statisticDao.update(_ as StatisticEntity) >> 1
    }

    void "increment when statistic was not created - create new statistic entry"() {
        when:
            statisticService.incrementStatistic("long_url")
        then:
            1 * statisticDao.createStatisticEntity(_ as StatisticEntity)
            0 * statisticDao.update(_ as StatisticEntity)
    }

    void "increment when statistic was created - update statistic"() {
        setup:
            statisticDao.getStatisticEntityByLink(_ as Long) >> statisticEntity
        when:
            statisticService.incrementStatistic(LinkUtil.getShortLinkById(1))
        then:
            0 * statisticDao.createStatisticEntity(_ as StatisticEntity)
    }

}

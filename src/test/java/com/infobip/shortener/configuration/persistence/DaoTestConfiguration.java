package com.infobip.shortener.configuration.persistence;

import com.infobip.shortener.dao.AccountDao;
import com.infobip.shortener.dao.ShortLinkDao;
import com.infobip.shortener.dao.StatisticDao;
import com.infobip.shortener.dao.impl.AccountDaoImpl;
import com.infobip.shortener.dao.impl.ShortLinkDaoImpl;
import com.infobip.shortener.dao.impl.StatisticDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DaoTestConfiguration {

    @Bean
    public AccountDao accountDao() {
        return new AccountDaoImpl();
    }

    @Bean
    public ShortLinkDao shortLinkDao() {
        return new ShortLinkDaoImpl();
    }

    @Bean
    public StatisticDao statisticDao() {
        return new StatisticDaoImpl();
    }
}

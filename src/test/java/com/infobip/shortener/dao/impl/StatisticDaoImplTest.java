package com.infobip.shortener.dao.impl;

import com.infobip.shortener.configuration.persistence.DaoTestConfiguration;
import com.infobip.shortener.configuration.persistence.DataSourceTestConfiguration;
import com.infobip.shortener.configuration.persistence.PersistenceTestConfiguration;
import com.infobip.shortener.dao.StatisticDao;
import com.infobip.shortener.entity.StatisticEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class,
        classes={DataSourceTestConfiguration.class, PersistenceTestConfiguration.class, DaoTestConfiguration.class})
public class StatisticDaoImplTest {

    @Autowired
    private StatisticDao statisticDao;

    @Test
    public void testCreateStatistic() {
        StatisticEntity statisticEntity = new StatisticEntity();
        statisticDao.createStatisticEntity(statisticEntity);
    }

    @Test
    public void testUpdateStatistic() {
        StatisticEntity statisticEntity = new StatisticEntity();
        statisticEntity.setLink(1L);
        statisticEntity.setRedirects(1L);
        statisticDao.createStatisticEntity(statisticEntity);
        statisticEntity.setRedirects(2L);
        Assert.assertNotEquals(statisticDao.update(statisticEntity), 0);
    }
}

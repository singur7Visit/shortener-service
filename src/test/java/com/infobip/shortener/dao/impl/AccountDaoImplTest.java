package com.infobip.shortener.dao.impl;

import com.infobip.shortener.configuration.persistence.DaoTestConfiguration;
import com.infobip.shortener.configuration.persistence.DataSourceTestConfiguration;
import com.infobip.shortener.configuration.persistence.PersistenceTestConfiguration;
import com.infobip.shortener.dao.AccountDao;
import com.infobip.shortener.entity.AccountEntity;
import com.infobip.shortener.exception.UserExistsException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class,
        classes={DataSourceTestConfiguration.class, PersistenceTestConfiguration.class, DaoTestConfiguration.class})
public class AccountDaoImplTest {

    @Autowired
    private AccountDao accountDao;

    @Test
    public void whenAccountIsCreatedAndNotDuplicatedThenCreate() {
        AccountEntity accountEntity = createTestAccountEntity();
        accountEntity = accountDao.createAccount(accountEntity);
        Assert.assertNotNull(accountEntity.getId());
    }

    @Test(expected = UserExistsException.class)
    public void whenAccountIsCreatedAndDuplicatedThenCreate() {
        AccountEntity accountEntity = createTestAccountEntity();
        accountDao.createAccount(accountEntity);
        accountDao.createAccount(accountEntity);
    }

    @Test
    public void testGetAccount() {
        AccountEntity accountEntity = createTestAccountEntity();
        accountDao.createAccount(accountEntity);
        Assert.assertNotNull(accountDao.getAccount("testAccount"));
    }

    private AccountEntity createTestAccountEntity() {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountId("testAccount");
        accountEntity.setPassword("testPassword");
        return accountEntity;
    }
}

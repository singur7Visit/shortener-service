package com.infobip.shortener.dao.impl;

import com.infobip.shortener.configuration.persistence.DaoTestConfiguration;
import com.infobip.shortener.configuration.persistence.DataSourceTestConfiguration;
import com.infobip.shortener.configuration.persistence.PersistenceTestConfiguration;
import com.infobip.shortener.dao.ShortLinkDao;
import com.infobip.shortener.entity.UrlEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class,
        classes={DataSourceTestConfiguration.class, PersistenceTestConfiguration.class, DaoTestConfiguration.class})
public class ShortLinkDaoImplTest {

    @Autowired
    private ShortLinkDao shortLinkDao;

    @Test
    public void testCreateUrl() {
        UrlEntity urlEntity = new UrlEntity();
        shortLinkDao.createUrlEntity(urlEntity);
    }

    @Test
    public void testGetUrl() {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setAccountId(1L);
        urlEntity.setStatus(302);
        shortLinkDao.createUrlEntity(urlEntity);
        Assert.assertNotNull(shortLinkDao.getLongUrl(1L));
    }
}
